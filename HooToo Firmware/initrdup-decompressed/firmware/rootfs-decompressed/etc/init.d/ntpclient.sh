#!/bin/sh
# description: Starts and stops the ntp daemons \
#	       used to provide ntp services.
#
# pidfile: /var/run/ntp.pid

# Const
PIDPATH=/var/run
PRGNAME=ntpclient
SRVNAME=ntpclient

# Source function library.
. /etc/init.d/vstfunc
# Check that ntp.conf exists.
[ -f /etc/ntp/ntp.cfg ] || exit 0

getserv()
{
        TIMESERV=`grep server /etc/ntp/ntp.cfg | awk -F'=' '{print $2}'`

        TIMESERV0=`echo $TIMESERV | awk -F':' '{print $1}'`
        TIMESERV1=`echo $TIMESERV | awk -F':' '{print $2}'`
        TIMESERV2=`echo $TIMESERV | awk -F':' '{print $3}'`
        TIMESERV3=`echo $TIMESERV | awk -F':' '{print $4}'`

        [ -n "$TIMESERV0" ] && SRVHOST="$SRVHOST -h $TIMESERV0"
        [ -n "$TIMESERV1" ] && SRVHOST="$SRVHOST -h $TIMESERV1"
        [ -n "$TIMESERV2" ] && SRVHOST="$SRVHOST -h $TIMESERV2"
        [ -n "$TIMESERV3" ] && SRVHOST="$SRVHOST -h $TIMESERV3"
}

# Function
start() {
    	#check program
	checkonly $PRGNAME
	if [ $? != 0 ]; then
		getserv
		TIMEINTER=`grep time /etc/ntp/ntp.cfg | awk -F'=' '{print $2}'`
		if [ "$SRVHOST" == "" ] || [ "$TIMEINTER" == "" ]; then
			return
		fi
		let TIMEINTER=TIMEINTER*3600
    		/bin/$PRGNAME -s -c 0 $SRVHOST -i $TIMEINTER &
		savesc 3 /usr/sbin/$PRGNAME $SRVNAME
	fi
    	
	return $?
}	
stop() {
	killproc $PRGNAME
	return $?
}	
restart() {
	stop
	start 
}
# Select	
case "$1" in
  start)
  	start
	;;
  stop)
  	stop
	;;
  restart)
  	restart
	;;
  *)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $?
