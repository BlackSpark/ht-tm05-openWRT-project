#! /bin/sh
# Const
PIDPATH=/var/run
PRGNAME=udhcpd
SRVNAME=udhcpd
netbad=/tmp/netbad
nodns=/etc/newfunction/nodns
# Source function library.
. /sbin/global.sh
. /etc/init.d/vstfunc
#. /etc/init.d/vstfunc

# Function
restart() {
	killproc udhcpd
	echo "option lease 86400" > /etc/udhcpd.conf
	echo "" > /var/udhcpd.leases
	dhcp=`nvram_get 2860 dhcpEnabled`
	if [ "$dhcp" = "1" ]; then
		start=`nvram_get 2860 dhcpStart`
		end=`nvram_get 2860 dhcpEnd`
		mask=`nvram_get 2860 dhcpMask`
		pd=`nvram_get 2860 dhcpPriDns`
		sd=`nvram_get 2860 dhcpSecDns`
		gw=`nvram_get 2860 dhcpGateway`
		lease=`nvram_get 2860 dhcpLease`
		config-udhcpd.sh -s $start
		config-udhcpd.sh -e $end
		config-udhcpd.sh -i $lan_if
		config-udhcpd.sh -m $mask
		if [ ! -f $nodns  -o ! -f $netbad ]; then 
		if [ "$pd" != "" -o "$sd" != "" ]; then
			config-udhcpd.sh -d $pd $sd
		fi
		fi
		if [ "$gw" != "" ]; then
			config-udhcpd.sh -g $gw
		fi
		if [ "$lease" != "" ]; then
			config-udhcpd.sh -t $lease
		fi
		config-udhcpd.sh -r 0
	fi
}
# Select
case "$1" in
  	restart)
  		restart
		;;
  	*)
		echo $"Usage: $0 {start|stop|restart}"
		exit 1
		;;
esac

exit $?

